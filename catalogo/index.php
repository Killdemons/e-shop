<?php
require_once '../shared/guard.php';
$title = 'Catalogo';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
$catalogo = $producto_model->select();
$categories = $categoria_model->select();
$search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
if (isset($search)) {
	if (isset($id)) {
		$catalogo = $producto_model->find_sub($search);
	}else {
		$catalogo = $producto_model->find_catalogo($search);
	}
}
?>
<!-- Inicio Categories-->
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2 sidebar1">
			<h3 class="text-center" style="color:white;"><i class="fas fa-th-list"></i> <?=$title?></h3>			
			<div id="MainMenu">
		        <div class="list-group panel">
		          <a href="/catalogo" class="list-group-item schedule-list" data-parent="#MainMenu">Todo</a>
		          <?php
		          if ($categories) {
		          	foreach ($categories as $categorie) {
		          		if ($categorie['name_sub'] == '') {
		          			echo "<a href='#" . $categorie['name'] . "' class='list-group-item schedule-list' data-toggle='collapse' data-parent='#MainMenu'>" . $categorie['name'] . "<i class='fa fa-caret-down'></i></a>";
		          			require __DIR__ . '/menu.php';
		          		}
		          	}
		          }
		          ?>
		        </div>
		    </div>
		</div>
		<div id="loadpartial" class="col-md-10">
			<table id="schedule" class="table-condensed table table-hover table-bordered dt-responsive responsive-display">
				<thead class="thead-light">
					<tr class="text-center">
						<th><i class="fas fa-barcode"></i> SKU</th>
						<th><i class="fas fa-user"></i> Nombre</th>
						<th><i class="fas fa-comment-dots"></i> Descripcion</th>
						<th><i class="fas fa-image"></i> Imagen</th>
						<th><i class="fas fa-dollar-sign"></i> Precio</th>
						<th><i class="fas fa-shopping-cart"></i> Carrito</th>
					</tr>
				</thead>
				<?php
				if ($catalogo) {
				    foreach ($catalogo as $producto) {
				    	if ((int)$producto['stock']>0) {
				    		require __DIR__ . '/row.php';
				        	echo "<td><a href='/catalogo/add_cart.php?id_produ=" . $producto['id'] . "&search=" . $search . "' class='btn btn-dark btn-sm mr-1'><i class='fas fa-pen-square'></i> Agregar</a>
							</td></tr>";
				    	}
				        
				    }
				}
				?>
	    	</table>
		</div>
	</div>
</div>
