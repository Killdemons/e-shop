<?php
require_once '../shared/guard.php';
$title = 'Check Out';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
date_default_timezone_set("America/Costa_Rica");
$id_user = $_SESSION['usuario_id'];
$carrito = $carrito_model->find($id_user);
$total = 0;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$fecha = date('Y/m/d H:i');
	$id_orden = $_SESSION['usuario_name'] . date('dsu');
	foreach ($carrito as $producto) {
		$checkout_model->insert_orden($id_orden, $producto['sku'], $producto['name'], $producto['description'], $producto['imagen'], $producto['price']);
		$producto_model->update_stock($producto['sku'], 1);
		$carrito_model->delete($producto['id']);
		$total += $producto['price'];
	}
	$checkout_model->insert_checkout($id_user, $fecha, $id_orden, $total);
	return header('Location: /catalogo/cart.php');
}

?>
<div class="container">
	<h1 class="text-center"><?=$title?></h1>
	<div id="loadpartial">
		<form method="POST">
			<?php
			if ($carrito) {
				$num = 1;
				foreach ($carrito as $producto) {
					echo "<h5>Producto " . $num++ . "</h5>";
					echo "<label>Nombre:</label>";
					echo "<label class='form-control'>" . $producto['name'] . "</label>";
					echo "<label>Monto:</label>";
					echo "<label class='form-control'>₡" . $producto['price'] . "</label>";
					$total += $producto['price'];
				}
				echo "<h5>Total a pagar:</h5>";
				echo "<label class='form-control' name='total'>₡" . $total . "</label>";
			}
			?>
			<div class="float-right">
				<button class="btn btn-dark" type="submit"><i class="fas fa-check"></i> Aceptar</button>
				<a class="btn btn-default btn-danger" href="/catalogo/cart.php"><i class="fas fa-ban"></i> Cancelar</a>
			</div>
		</form>
	</div>