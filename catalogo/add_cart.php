<?php
require_once '../shared/guard.php';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';

$id_produ = filter_input(INPUT_GET, 'id_produ', FILTER_SANITIZE_STRING);
$search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$id_user = $_SESSION['usuario_id'];
if (isset($id_produ)) {
	$producto = $producto_model->find($id_produ);
	$carrito_model->insert( $id_user, $producto['sku'], $producto['name'], $producto['description'], $producto['imagen'], $producto['price']);
	if ($search == '') {
		return header('Location: /catalogo');
	}
	return header('Location: /catalogo/index.php?search=' . $search);
}
elseif(isset($id)){
	$carrito_model->delete($id);
	return header('Location: /catalogo/cart.php');
}
?>