<?php
require_once '../shared/guard.php';
$title = 'Carrito';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';

$id_user = $_SESSION['usuario_id'];
$carrito = $carrito_model->find($id_user);
?>
<div class="container">
	<h1 class="text-center"><?=$title?></h1>
	<div id="loadpartial">
		<table id="schedule" class="table-condensed table table-hover table-bordered dt-responsive responsive-display">
			<thead class="thead-light">
				<tr class="text-center">
					<th>SKU</th>
					<th>Nombre</th>
					<th>Descripcion</th>
					<th>Imagen</th>
					<th>Precio</th>
					<th>Carrito</th>
				</tr>
			</thead>
			<?php
			if ($carrito) {
			    foreach ($carrito as $producto) {
			        require __DIR__ . '/row.php';
			        echo "
			        	<td>
							<a href='/catalogo/add_cart.php?id=" . $producto['id'] . "' class='btn btn-danger btn-sm mr-1'><i class='fas fa-pen-square'></i> Borrar</a>
						</td>
					</tr>";
			    }
			}else{
				echo "<tr><td colspan='6' class='text-center'>No ha añadido productos al carrito</td></tr>";
			}
			?>
		</table>
		<a class="btn btn-dark float-right" href="/catalogo/checkout.php"><i class="fas fa-clipboard-check"></i> Check Out</a>
	</div>
</div>