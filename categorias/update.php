<?php
require_once '../shared/guard.php';
require_once '../shared/guard_admin.php';
$title = 'Editar Categoria';
require_once '../shared/header.php';
require_once '../shared/db.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$categoria = $categoria_model->find($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $name_sub = filter_input(INPUT_POST, 'name_sub', FILTER_SANITIZE_STRING);
    $categoria_model->update($id, $name, $name_sub);
    return header('Location: /categorias');
}
?>
<div class="container">
  <h1><?=$title?></h1>
<?php require_once __DIR__ . '/form.php'; ?>
</div>
