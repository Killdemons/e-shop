<?php
require_once '../shared/guard.php';
require_once '../shared/guard_admin.php';
$title = 'Crear Categoria';
require_once '../shared/header.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../shared/db.php';
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $name_sub = filter_input(INPUT_POST, 'name_sub', FILTER_SANITIZE_STRING);
    $validar = $categoria_model->validation($name);
    if (!$validar == null || !empty($validar)) {
        echo "<div class='alert alert-success' role='alert'>
              Ese nombre ya esta registrado. Favor verificar que no es la misma categoria o cambie el nombre.
            </div>";
    }else{
    	$categoria_model->insert($name, $name_sub);
    	return header('Location: /categorias');
    }
}
?>
<div class="container">
  <h1><?=$title?></h1>
<?php require_once __DIR__ . '/form.php'; ?>
</div>
