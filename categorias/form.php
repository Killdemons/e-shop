<form method="POST">
  <div class="form-group">
    <label for="name">Nombre</label>
    <input type="text" class="form-control" placeholder="Name" name="name" value="<?=$categoria['name'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="name_sub"><i class="fas fa-grip-horizontal"></i> SubCategoria</label>
    <select class="form-control" name="name_sub" value="<?=$categoria['name_sub'] ?? ''?>">
      <?php
      require_once '../shared/db.php';
      $cates = $categoria_model->select();
      if ($cates) {
        echo '<option></option>';
        foreach ($cates as $cate) {
          if ($cate['name'] == $categoria['name_sub']) {
        		echo '<option selected>' . $cate['name'] . '</option>';
        	}else{
            echo '<option>' . $cate['name'] . '</option>';
          }
        }
      }
      ?>
    </select>
  </div>
  <button class="btn btn-dark" type="submit"><i class="fas fa-check-circle"></i> Aceptar</button>
  <a class="btn btn-default btn-danger" href="/categorias"><i class="fas fa-ban"></i> Cancelar</a>
</form>
