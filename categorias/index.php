<?php
  require_once '../shared/guard.php';
  require_once '../shared/guard_admin.php';
  $title = 'Categorias';
  require_once '../shared/header.php';
  require_once '../shared/db.php';
  $categorias = $categoria_model->select();
?>
<div class="container">
  <h1 class="text-center"><?=$title?></h1>
  <table class="table table-striped table-bordered">
    <thead class="thead-dark">
      <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>SubCategoria</th>
        <th class="text-center"><a href="/categorias/create.php" class="btn btn-success btn-sm"><i class="fas fa-plus-circle"></i> Nuevo</a></th>
      </tr>
    </thead>
<?php
if ($categorias) {
    foreach ($categorias as $categoria) {
        require __DIR__ . '/row.php';
    }
}
?>
  </table>
</div>
