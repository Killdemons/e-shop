<?php
require_once '../shared/guard.php';
require_once '../shared/guard_admin.php';
$title = 'Eliminar Categoria';
require_once '../shared/header.php';
require_once '../shared/db.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$categoria = $categoria_model->find($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $categoria_model->delete($id);
    return header('Location: /categorias');
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <p>¿Está seguro de eliminar la categoria con el id <?=$id?></p>
  <form method="POST">
    <button class="btn btn-dark" type="submit"><i class="fas fa-check-circle"></i> Aceptar</button>
    <a class="btn btn-danger" href="/categorias"><i class="fas fa-ban"></i> Cancelar</a>
  </form>
</div>
