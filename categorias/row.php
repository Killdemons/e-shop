<tr>
  <td><?=$categoria['id']?></td>
  <td><?=$categoria['name']?></td>
  <td><?=$categoria['name_sub']?></td>
  <td>
    <a href='/categorias/update.php?id=<?=$categoria['id']?>' class='btn btn-dark btn-sm mr-1'><i class="fas fa-pen-square"></i> Editar</a>
    <a href='/categorias/delete.php?id=<?=$categoria['id']?>' class='btn btn-danger btn-sm'><i class="fas fa-minus-circle"></i> Eliminar</a>
  </td>
</tr>
