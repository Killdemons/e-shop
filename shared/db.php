<?php

require_once __DIR__ . '/../Db/PgConnection.php';
require_once __DIR__ . '/../Models/Usuario.php';
require_once __DIR__ . '/../Models/Categoria.php';
require_once __DIR__ . '/../Models/Producto.php';
require_once __DIR__ . '/../Models/Carrito.php';
require_once __DIR__ . '/../Models/Checkout.php';

use Db\PgConnection;

$con = new PgConnection('postgres', 'bryan2748245', 'eshop', 5432, 'localhost');
$con->connect();

$usuario_model = new Models\Usuario($con);
$categoria_model = new Models\Categoria($con);
$producto_model = new Models\Producto($con);
$carrito_model = new Models\Carrito($con);
$checkout_model = new Models\Checkout($con);
