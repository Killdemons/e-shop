<?php
require_once __DIR__ . '/sessions.php';
if ($_SESSION['usuario_type'] != 't') {
    return header('Location: /home');
}