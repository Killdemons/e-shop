<?php
  require_once __DIR__ . '/sessions.php';
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href=""><i class="fab fa-shopware"></i> E-Shop</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <?php
        $menu = [];
        if (isset($_SESSION['usuario_id']) && $_SESSION['usuario_type'] == 'f') {
          $menu = [
            '<i class="fas fa-home"></i> Home' => '/',
            '<i class="fas fa-th-list"></i> Catalogo' => '/catalogo',
            '<i class="fas fa-history"></i> Historial' => '/home/historial.php',
          ];
        }
        if (isset($_SESSION['usuario_id']) && $_SESSION['usuario_type'] == 't') {
          $admin = [
            '<i class="fas fa-home"></i> Home' => '/home/index_admin.php',
            '<i class="fas fa-th-list"></i> Categorias' => '/categorias',
            '<i class="fab fa-product-hunt"></i> Productos' => '/productos',
          ];
          $menu = array_merge($menu, $admin);
        }
        if (isset($_SESSION['usuario_id']) || !empty($_SESSION['usuario_id'])) {
            foreach ($menu as $key => $value) {
                echo "<li class='nav-item'>
                      <a class='nav-link' href='$value'>$key</a>
                    </li>";
            }
        }
        ?>
    </ul>
    <?php
      if (isset($_SESSION['usuario_id']) || !empty($_SESSION['usuario_id'])) {
    ?>
    <ul class="nav navbar-nav navbar-right mr-2">
      <div class="dropdown">
        <button class="btn btn-outline-light dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user"></i> <?=$_SESSION['usuario_name'] ?? ''?>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
          <?php
          if (isset($_SESSION['usuario_id']) && $_SESSION['usuario_type'] == 'f') {
          ?>
          <li><a class="btn btn-outline-light dropdown-item" href='/catalogo/cart.php'>
            <i class="fas fa-shopping-cart"></i> Carrito
          </a></li>
          <?php
          }
          ?>
          <li><a class="btn btn-outline-light dropdown-item" href='/seguridad/logout.php'>
            <i class="fas fa-sign-out-alt"></i> LogOut
          </a></li>
        </div>
      </div>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
    </ul>
    <?php
    } elseif ($_SERVER['REQUEST_URI'] == '/seguridad/login.php') {
    ?>
    <ul class="nav navbar-nav navbar-right">
      <li><a class="btn btn-outline-light" href='/seguridad/signup.php'><i class="fas fa-user-plus"></i> Sign Up</a></li>
    </ul>
    <?php
    }
    ?>
  </div>
</nav>


