-- Creacion Database: eshop

DROP DATABASE if exists eshop;

CREATE DATABASE eshop

-- Creacion Table: users

DROP TABLE if exists users;

CREATE TABLE users
(
    id serial NOT NULL ,
    email character varying NOT NULL,
    password character varying NOT NULL,
    name character varying NOT NULL,
    phone integer NOT NULL,
    address character varying NOT NULL,
    type boolean NOT NULL default false,
    PRIMARY KEY (id, email),
    UNIQUE (email)
)

-- Creacion Table: categories

DROP TABLE if exists categories;

CREATE TABLE categories
(
    id serial NOT NULL,
    name character varying NOT NULL,
    name_sub character varying,
    PRIMARY KEY (id),
    UNIQUE (name)
)

-- Creacion Table: productos

DROP TABLE if exists products;

CREATE TABLE products
(
    id serial NOT NULL,
    sku character varying NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL,
    imagen character varying NOT NULL,
    categorie character varying NOT NULL,
    stock integer NOT NULL,
    price integer NOT NULL,
    PRIMARY KEY (id, sku),
    UNIQUE (sku)
)

-- Creacion Table: cart

DROP TABLE if exists cart;

CREATE TABLE cart
(
    id serial NOT NULL,
    id_client integer NOT NULL,
    sku character varying NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL,
    imagen character varying NOT NULL,
    price integer NOT NULL,
    PRIMARY KEY (id)
)

-- Creacion Table: orden

DROP TABLE if exists orden;

CREATE TABLE orden
(
    id serial NOT NULL,
    id_orden character varying NOT NULL,
    sku character varying NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL,
    imagen character varying NOT NULL,
    price integer NOT NULL,
    PRIMARY KEY (id)
)

-- Creacion Table: checkout

DROP TABLE if exists checkout;

CREATE TABLE checkout
(
    id serial NOT NULL,
    id_user integer NOT NULL,
    date timestamp with time zone NOT NULL,
    id_orden character varying NOT NULL,
    total integer NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (id_orden)
)

--Insertar Usuario administrador
INSERT INTO users(email, password, name, phone, address, type) VALUES ('admin@utn.ac.cr', MD5('admin'), 'Administrador', 24600011, 'Ciudad Quesada', true);

--Insertar Usuario cliente
INSERT INTO users(username, password, name, phone, address) VALUES ('client@utn.ac.cr', MD5('client'), 'Cliente', 24601100, 'Ciudad Quesada');

-- Verificacion de inserts correctamente
SELECT * FROM users;


