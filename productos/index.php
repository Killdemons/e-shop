<?php
  require_once '../shared/guard.php';
  require_once '../shared/guard_admin.php';
  $title = 'Productos';
  require_once '../shared/header.php';
  require_once '../shared/db.php';
  $productos = $producto_model->select();
?>
<div class="container">
  <h1 class="text-center"><?=$title?></h1>
  <table class="table table-striped table-bordered">
    <thead class="thead-dark">
      <tr>
        <th>SKU</th>
        <th>Nombre</th>
        <th>Descripcion</th>
        <th>Imagen</th>
        <th>Categoria</th>
        <th>Stock</th>
        <th>Precio</th>
        <th class="text-center"><a href="/productos/create.php" class="btn btn-success btn-sm"><i class="fas fa-plus-circle"></i> Nuevo</a></th>
      </tr>
    </thead>
<?php
if ($productos) {
    foreach ($productos as $producto) {
        require __DIR__ . '/row.php';
    }
}
?>
  </table>
</div>
