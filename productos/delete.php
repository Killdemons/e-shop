<?php
require_once '../shared/guard.php';
require_once '../shared/guard_admin.php';
$title = 'Eliminar Producto';
require_once '../shared/header.php';
require_once '../shared/db.php';

$sku = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $producto_model->delete($sku);
    return header('Location: /productos');
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <p>¿Está seguro de eliminar el producto con el SKU <?=$sku?>?</p>
  <form method="POST">
    <button class="btn btn-dark" type="submit"><i class="fas fa-check-circle"></i> Aceptar</button>
    <a class="btn btn-default btn-danger" href="/productos"><i class="fas fa-ban"></i> Cancelar</a>
  </form>
</div>
