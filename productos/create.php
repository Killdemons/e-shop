<?php
require_once '../shared/guard.php';
require_once '../shared/guard_admin.php';
$title = 'Crear Producto';
require_once '../shared/header.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../shared/db.php';
    $sku = filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_STRING);
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    $imagen = filter_input(INPUT_POST, 'imagen', FILTER_SANITIZE_STRING);
    if ($imagen == null || empty($imagen)) {
        $imagen = $producto['imagen'];
    }
    $categorie = filter_input(INPUT_POST, 'categorie', FILTER_SANITIZE_STRING);
    $stock = filter_input(INPUT_POST, 'stock', FILTER_SANITIZE_STRING);
    $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
    $validar = $producto_model->validation($sku);
    if (!$validar == null || !empty($validar)) {
        echo "<div class='alert alert-success' role='alert'>
              Ese SKU ya esta registrado. Favor verificar que no es el mismo producto o cambie el SKU.
            </div>";
    }else{
        $producto_model->insert($sku, $name, $description, $imagen, $categorie, $stock, $price);
        return header('Location: /productos');
    }
}
?>
<div class="container">
  <h1><?=$title?></h1>
<?php require_once __DIR__ . '/form.php'; ?>
</div>
