<tr>
  <td><?=$producto['sku']?></td>
  <td><?=$producto['name']?></td>
  <td><?=$producto['description']?></td>
  <td class="text-center"><img src="../assets/imgs/<?=$producto['imagen']?>"></td>
  <td><?=$producto['categorie']?></td>
  <td><?=$producto['stock']?></td>
  <td><?=$producto['price']?></td>
  <td>
    <a href='/productos/update.php?id=<?=$producto['id']?>' class='btn btn-dark btn-sm mr-1'><i class="fas fa-pen-square"></i> Editar</a>
    <a href='/productos/delete.php?id=<?=$producto['sku']?>' class='btn btn-danger btn-sm'><i class="fas fa-minus-circle"></i> Eliminar</a>
  </td>
</tr>
