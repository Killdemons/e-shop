<?php
require_once '../shared/guard.php';
require_once '../shared/guard_admin.php';
$title = 'Editar Producto';
require_once '../shared/header.php';
require_once '../shared/db.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$producto = $producto_model->find($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $sku = filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_STRING);
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    $imagen = filter_input(INPUT_POST, 'imagen', FILTER_SANITIZE_STRING);
    $categorie = filter_input(INPUT_POST, 'categorie', FILTER_SANITIZE_STRING);
    $stock = filter_input(INPUT_POST, 'stock', FILTER_SANITIZE_STRING);
    $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
    $producto_model->update($id, $sku, $name, $description, $imagen, $categorie, $stock, $price);
    return header('Location: /productos');
}
?>
<div class="container">
  <h1><?=$title?></h1>
<?php require_once __DIR__ . '/form.php'; ?>
</div>
