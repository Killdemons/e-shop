<form method="POST">
	<div class="form-group">
		<label for="sku"><i class="fas fa-barcode"></i> SKU</label>
    <input type="text" class="form-control" placeholder="SKU" name="sku" value="<?=$producto['sku'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="name"><i class="fas fa-user"></i> Nombre</label>
    <input type="text" class="form-control" placeholder="Nombre" name="name" value="<?=$producto['name'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="description"><i class="fas fa-comment-dots"></i> Descripcion</label>
    <input type="text" class="form-control" placeholder="Descripcion" name="description" value="<?=$producto['description'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="imagen"><i class="fas fa-image"></i> Imagen</label>
    <div class="input-group input-file" name="Fichier1">
      <span class="input-group-btn">
            <button class="btn btn-default btn-choose" type="button">Buscar</button>
        </span>
        <input type="text" class="form-control" placeholder='Imagen' name="imagen" value="<?=$producto['imagen'] ?? ''?>"/>
        <span class="input-group-btn">
             <button class="btn btn-warning btn-reset" type="button">Limpiar</button>
        </span>
    </div>
  </div>
  <?php
  require_once '../assets/js/imagen.js';
  ?>
  <div class="form-group">
    <label for="categorie"><i class="fas fa-grip-horizontal"></i> Categoria</label>
    <select class="form-control" name="categorie" value="<?=$producto['categorie'] ?? ''?>" required>
      <?php
      require_once '../shared/db.php';
      $categorias = $categoria_model->select();
      if ($categorias) {
        foreach ($categorias as $categoria) {
          if ($producto['categorie'] == $categoria['name']) {
            echo '<option selected>' . $categoria['name'] . '</option>';
          }else{
            echo '<option>' . $categoria['name'] . '</option>';
          }
          
        }
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label for="stock"><i class="fas fa-calculator"></i> Stock</label>
    <input type="number" class="form-control" placeholder="Stock" name="stock" value="<?=$producto['stock'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="price"><i class="fas fa-dollar-sign"></i> Precio</label>
    <input type="number" class="form-control" placeholder="Precio" name="price" value="<?=$producto['price'] ?? ''?>" required>
  </div>
  <div class="float-right">
    <button class="btn btn-dark" type="submit"><i class="fas fa-check"></i> Aceptar</button>
    <a class="btn btn-default btn-danger" href="/productos"><i class="fas fa-ban"></i> Cancelar</a>
  </div>
  
</form>