<?php
require_once '../shared/guard.php';
$title = 'Detalles';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';

$id_orden = filter_input(INPUT_GET, 'id_orden', FILTER_SANITIZE_STRING);
$detalles = $checkout_model->details($id_orden);
?>
<div class="container">
	<h1 class="text-center"><?=$title?> de la orden <?=$id_orden?></h1>
	<div id="loadpartial">
		<table id="schedule" class="table-condensed table table-hover table-bordered dt-responsive responsive-display">
			<thead class="thead-dark">
				<tr class="text-center">
					<th>SKU</th>
					<th>Nombre</th>
					<th>Descripcion</th>
					<th>Imagen</th>
					<th>Precio</th>
				</tr>
			</thead>
			<?php
			if ($detalles) {
			    foreach ($detalles as $detalle) {
			        require __DIR__ . '/row.php';
			    }
			}
			?>
		</table>
		<a class="btn btn-dark float-right" href="/home/historial.php"><i class="fas fa-arrow-circle-left"></i> Atras</a>
	</div>
</div>