<?php
require_once '../shared/guard.php';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
$title = 'Estadisticas';
$producto = $checkout_model->count($_SESSION['usuario_id']);
?>
<h1 class="text-center">Bienvenido a E-Shop</h1>
<h3 class="text-center"><?=$title?></h3>
<div class="container table-responsive">
  <table class="table table-striped table-bordered">
    <thead class="thead-dark">
      <tr>
        <th>Productos</th>
        <th>Total</th>
      </tr>
    </thead>
    <tr>
      <td><?=$producto['count']?></td>
      <td><?=$producto['sum'] ?? 0?></td>
    </tr>
  </table>
</div>

<?php
require_once '../shared/footer.php';
?>
