<?php
require_once '../shared/guard.php';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
$title = 'Estadisticas';
$checkout = $checkout_model->find_checkout($_SESSION['usuario_id']);
date_default_timezone_set("America/Costa_Rica");
?>
<h1 class="text-center">Bienvenido a E-Shop</h1>
<h3 class="text-center"><?=$title?></h3>
<div class="container table-responsive">
  <table class="table table-striped table-bordered">
    <thead class="thead-dark">
      <tr>
        <th>Fecha</th>
        <th>Total</th>
        <th>Detalles</th>
      </tr>
    </thead>
    <tr>
      <?php
      foreach ($checkout as $orden) {
        echo "<tr><td>" . date("d-m-Y", strtotime($orden['date'])) . "</td>";
        echo "<td>" . $orden['total'] . "</td>";
        echo "<td><a href='/home/detalles.php?id_orden=" . $orden['id_orden'] . "' class='btn btn-info btn-sm'><i class='fas fa-info-circle'></i> Ver</a></td></tr>";
      }
      ?>
    </tr>
  </table>
</div>

<?php
require_once '../shared/footer.php';
?>