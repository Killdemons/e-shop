<?php
require_once '../shared/guard.php';
require_once '../shared/guard_admin.php';
$title = 'Estadisticas';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';

$usuario = $usuario_model->count();
$producto = $producto_model->count();
$total = $checkout_model->total();
?>
<h1 class="text-center">Bienvenido a E-Shop</h1>
<h3 class="text-center"><?=$title?></h3>
<div class="container table-responsive">
  <table class="table table-striped table-bordered">
    <thead class="thead-dark">
      <tr>
        <th>Clientes</th>
        <th>Productos</th>
        <th>Total</th>
      </tr>
    </thead>
    <tr>
      <td><?=$usuario['count']?></td>
      <td><?=$producto['count']?></td>
      <td><?=$total['sum']?></td>
    </tr>
  </table>
</div>

<?php
require_once '../shared/footer.php';
?>