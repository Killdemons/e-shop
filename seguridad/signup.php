<?php
$title = 'Registro';
require_once '../shared/header.php';
require_once '../shared/db.php';

$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
$phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
$address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
$password_verification = filter_input(INPUT_POST, 'password_verification', FILTER_SANITIZE_STRING);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $validar = $usuario_model->validation($email);
  if (!$validar == null || !empty($validar)) {
    echo "<div class='alert alert-danger' role='alert'>
            Email digitado ya se encuentra registrado.
          </div>";
  }
  else if ($password == $password_verification) {
    $usuario_model->insert($email, $password, $name, $phone, $address);
    $user = $usuario_model->login($email, $password);
    if ($user != null) {
          $_SESSION['usuario_id'] = $user['id'];
          return header('Location: /home');
    }
  }else {
    echo "<div class='alert alert-danger' role='alert'>
            Contraseñas no coinciden.
          </div>";
  }
      
}
?>
<div class="container">
  <h1 class="text-center"><?=$title?></h1>
  <div class="row">
    <div class="col-md-12">
      <form method="POST">
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" aria-describedby="usernameHelp" placeholder="Email" name="email" value="<?=$email ?? ''?>">
        </div>
        <div class="form-group">
          <label for="name">Nombre Completo</label>
          <input type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Full Name" name="name" value="<?=$name ?? ''?>">
        </div>
        <div class="form-group">
          <label for="phone">Telefono</label>
          <input type="number" class="form-control" id="phone" aria-describedby="nameHelp" placeholder="Phone" name="phone" value="<?=$phone ?? ''?>">
        </div>
        <div class="form-group">
          <label for="address">Direccion</label>
          <input type="text" class="form-control" id="address" aria-describedby="nameHelp" placeholder="Address" name="address" value="<?=$address ?? ''?>">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password" placeholder="Password" name="password">
        </div>
        <div class="form-group">
          <label for="password_verification">Re-enter password</label>
          <input type="password" class="form-control" id="password_verification" placeholder="Password" name="password_verification">
        </div>
        <div class="form-group float-right">
          <button class="btn btn-dark" type="submit">Sign In <i class="fas fa-sign-in-alt"></i></button>
          <a class="btn btn-outline-dark" href="/"><i class="fas fa-ban"></i> Cancel</a>
        </div>
      </form>
    </div>
  </div>
</div>
