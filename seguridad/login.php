<?php
$title = 'Login';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/sessions.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $user = $usuario_model->login($email, $password);
    if ($user != null) {
          $_SESSION['usuario_id'] = $user['id'];
          $_SESSION['usuario_name'] = $user['name'];
          $_SESSION['usuario_type'] = $user['type'];
          if ($_SESSION['usuario_type'] == 't') {
            return header('Location: /home/index_admin.php');
          }
          return header('Location: /home');
    } else {
          echo "<div class='alert alert-danger' role='alert'>
                Usuario o contraseña inválido!
              </div>";
    }
}
?>
<div class="container">
  <h1 class="text-center"><?=$title?></h1>
  <div class="row">
    <div class="col-md-12">
      <form method="POST">
        <div class="form-group">
          <label for="email"><i class="fas fa-user"></i> Email</label>
          <input type="email" class="form-control" id="email" aria-describedby="usernameHelp" placeholder="Email" name="email" value="<?=$email ?? ''?>">
        </div>
        <div class="form-group">
          <label for="password"><i class="fas fa-key"></i> Password</label>
          <input type="password" class="form-control" id="password" placeholder="Password" name="password">
        </div>
        <button class="btn btn-dark float-right" type="submit">Sign In <i class="fas fa-sign-in-alt"></i></button>
      </form>
    </div>
  </div>
</div>
