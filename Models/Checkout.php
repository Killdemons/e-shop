<?php
namespace Models {
    class Checkout
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find_checkout($id_user)
        {
            return $this->connection->runQuery('SELECT * FROM checkout WHERE id_user = $1', [$id_user]);
        }    

        public function select_orden()
        {
            return $this->connection->runQuery('SELECT * FROM orden ORDER BY id');
        }

        public function total()
        {
            return $this->connection->runQuery('SELECT SUM(price) FROM orden')[0];
        }

        public function count($id)
        {
            $result = $this->connection->runQuery('SELECT COUNT(o.id_orden), SUM(o.price) FROM orden o, checkout c WHERE c.id_user = $1 AND o.id_orden = c.id_orden', [$id]);
            return $result[0];
        }

        public function details($id_orden)
        {
            return $this->connection->runQuery('SELECT * FROM orden WHERE id_orden = $1 ORDER BY id',[$id_orden]);
        }

        public function insert_checkout($id_user, $fecha, $id_orden, $total)
        {
            $this->connection->runStatement('INSERT INTO checkout(id_user, date, id_orden, total) VALUES ($1, $2, $3, $4)', [$id_user, $fecha, $id_orden, $total]);
        }

        public function insert_orden($id_orden, $sku, $name, $description, $imagen, $price)
        {
            $this->connection->runStatement('INSERT INTO orden(id_orden, sku, name, description, imagen, price) VALUES ($1, $2, $3, $4, $5, $6)', [$id_orden, $sku, $name, $description, $imagen, $price]);
        }

        public function update($id, $name, $name_sub)
        {
            $this->connection->runStatement('UPDATE categories SET name = $2, name_sub = $3 WHERE id = $1', [$id, $name, $name_sub]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM categories WHERE id = $1', [$id]);
        }
    }
}