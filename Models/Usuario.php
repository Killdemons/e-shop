<?php

namespace Models {
    class Usuario
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function count()
        {
            $result = $this->connection->runQuery('SELECT COUNT(*) FROM users');
            return $result[0];
        }

        public function validation($email)
        {
            $result = $this->connection->runQuery('SELECT * FROM users WHERE email = $1', [$email]);
            return $result[0];
        }

        public function login($email, $password)
        {
          $result = $this->connection->runQuery('SELECT * FROM users WHERE email = $1 and password = md5($2)', [$email, $password]);
          return $result[0];
        }

        public function insert($email, $password, $name, $phone, $address)
        {
            $sql = "INSERT INTO users(email, password, name, phone, address) VALUES ($1, md5($2), $3, $4, $5)";
            $this->connection->runStatement($sql, [$email, $password, $name, $phone, $address]);
        }
    }
}


