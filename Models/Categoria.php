<?php
namespace Models {
    class Categoria
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM categories WHERE id = $1', [$id])[0];
        }

        public function validation($name)
        {
            return $this->connection->runQuery('SELECT * FROM categories WHERE name = $1', [$name])[0];
        }

        public function find_sub($name)
        {
            return $this->connection->runQuery('SELECT * FROM categories WHERE name_sub = $1', [$name]);
        }        

        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM categories ORDER BY id');
        }

        public function insert($name, $name_sub)
        {
            $this->connection->runStatement('INSERT INTO categories(name, name_sub) VALUES ($1, $2)', [$name, $name_sub]);
        }

        public function update($id, $name, $name_sub)
        {
            $this->connection->runStatement('UPDATE categories SET name = $2, name_sub = $3 WHERE id = $1', [$id, $name, $name_sub]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM categories WHERE id = $1', [$id]);
        }
    }
}
