<?php
namespace Models {
    class Producto
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM products WHERE id = $1', [$id])[0];
        }

        public function validation($sku)
        {
            return $this->connection->runQuery('SELECT * FROM products WHERE sku = $1', [$sku])[0];
        }

        public function find_catalogo($categorie)
        {
            return $this->connection->runQuery('SELECT * FROM products WHERE categorie = $1 ORDER BY id', [$categorie]);
        }

        public function find_sub($subcategorie)
        {
            $result = [];
            $subcategories = $this->connection->runQuery('SELECT * FROM categories WHERE name_sub = $1 ORDER BY id', [$subcategorie]);
            foreach ($subcategories as $categorie) {
                $result = array_merge($result, $this->connection->runQuery('SELECT * FROM products WHERE categorie = $1 ORDER BY id', [$categorie['name']]));
            }
            return $result;
        }

        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM products ORDER BY id');
        }

        public function cronjob()
        {
            return $this->connection->runQuery('SELECT * FROM products WHERE stock<100 ORDER BY id');
        }

        public function count()
        {
            $result = $this->connection->runQuery('SELECT COUNT(*) FROM products');
            return $result[0];
        }

        public function insert( $sku, $name, $description, $imagen, $categorie, $stock, $price)
        {
            $this->connection->runStatement('INSERT INTO products(sku, name, description, imagen, categorie, stock, price) VALUES ($1, $2, $3, $4, $5, $6, $7)', [$sku, $name, $description, $imagen, $categorie, $stock, $price]);
        }

        public function update($id, $sku, $name, $description, $imagen, $categorie, $stock, $price)
        {
            $this->connection->runStatement('UPDATE products SET sku = $2, name = $3, description = $4, imagen = $5, categorie = $6, stock = $7, price = $8  WHERE id = $1', [$id, $sku, $name, $description, $imagen, $categorie, $stock, $price]);
        }

        public function update_stock($sku, $stock)
        {
            $result = $this->connection->runQuery('SELECT stock FROM products WHERE sku = $1', [$sku])[0];
            $this->connection->runStatement('UPDATE products SET stock = $2 WHERE sku = $1', [$sku, ((int)$result['stock']) - $stock]);
        }

        public function delete($sku)
        {
            $this->connection->runStatement('DELETE FROM products WHERE sku = $1', [$sku]);
        }
    }
}