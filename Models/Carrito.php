<?php
namespace Models {
    class Carrito
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id_client)
        {
            return $this->connection->runQuery('SELECT * FROM cart WHERE id_client = $1 ORDER BY sku', [$id_client]);
        }

        public function count()
        {
            $result = $this->connection->runQuery('SELECT COUNT(*) FROM cart');
            return $result[0];
        }

        public function insert( $id_client, $sku, $name, $description, $imagen, $price)
        {
            $this->connection->runStatement('INSERT INTO cart(id_client, sku, name, description, imagen, price) VALUES ($1, $2, $3, $4, $5, $6)', [$id_client, $sku, $name, $description, $imagen, $price]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM cart WHERE id = $1', [$id]);
        }

        public function clear($id_client)
        {
            $this->connection->runStatement('DELETE FROM cart WHERE id_client = $1', [$id_client]);
        }
    }
}